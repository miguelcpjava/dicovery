package br.com.discovery.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @description Classe de valores referenciando
 * a uma informação por exemplo;
 * 
 * Info = Dolar
 * O valor da informação = 3.14 no dia 25/05/2015 (Não real)
 * 
 *  Para que possamos montar o gráfico
 *  
 * @author Miguel Lima
 *
 */
@Entity
@Table(name="valores")
public class InfoValores implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8330940890159399755L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	@Column(name="valor")
	private double valorinfo;
	@Temporal(TemporalType.DATE)
	@Column(name="datacadastro")
	private Date datacadastro;
	@ManyToOne(optional = false)
	@JoinColumn(name="informacao")
	private Info informacao;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public double getValorinfo() {
		return valorinfo;
	}
	public void setValorinfo(double valorinfo) {
		this.valorinfo = valorinfo;
	}
	public Date getDatacadastro() {
		return datacadastro;
	}
	public void setDatacadastro(Date datacadastro) {
		this.datacadastro = datacadastro;
	}
	public Info getInformacao() {
		return informacao;
	}
	public void setInformacao(Info informacao) {
		this.informacao = informacao;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((datacadastro == null) ? 0 : datacadastro.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((informacao == null) ? 0 : informacao.hashCode());
		long temp;
		temp = Double.doubleToLongBits(valorinfo);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InfoValores other = (InfoValores) obj;
		if (datacadastro == null) {
			if (other.datacadastro != null)
				return false;
		} else if (!datacadastro.equals(other.datacadastro))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (informacao == null) {
			if (other.informacao != null)
				return false;
		} else if (!informacao.equals(other.informacao))
			return false;
		if (Double.doubleToLongBits(valorinfo) != Double
				.doubleToLongBits(other.valorinfo))
			return false;
		return true;
	}
	
	
}
