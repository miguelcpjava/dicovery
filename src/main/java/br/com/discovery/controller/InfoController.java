package br.com.discovery.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import br.com.discovery.dao.InfoDAO;
import br.com.discovery.model.Info;

@ManagedBean
@ViewScoped
public class InfoController implements Serializable{

	private static final long serialVersionUID = 5307137315445854379L;
	private Long infoID;
	private Info selectedInfo;
	private InfoDAO infDAO;
	public FacesMessage msg;
	private List<Info> InfoForAutocomplete;
	private List<Info> listInfoForAutoComplete;

	
	public InfoController() {
		selectedInfo = new Info();
		infDAO = new InfoDAO();
		listInfoForAutoComplete = new ArrayList<Info>();
		System.out.println("Incializando os objetos de Info...");
	}
	
	public List<Info> getListofInfos() {
	       if ((InfoForAutocomplete == null) || InfoForAutocomplete.isEmpty()) {
	           InfoForAutocomplete = infDAO.getListarTodos();
	       }
	       return InfoForAutocomplete;
	   }
	
	public void cleanNewInfo(){
		selectedInfo = null;
		FacesContext.getCurrentInstance().addMessage("add", new FacesMessage(FacesMessage.SEVERITY_INFO, "Pode adicionar uma nova Informação!", null));
		 selectedInfo = new Info();
	}
	
	public void salvarInfo(){
		
		try{
			infDAO.addInfo(selectedInfo);
			msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Dados salvos com sucesso!", null);
            FacesContext.getCurrentInstance().addMessage("add", msg);
            //Metodo abaixo é realizado a limpeza do formulario quando salva.
            cleanNewInfo();
		}catch(Exception e ){
			e.printStackTrace();
			e.getMessage();
            e.getLocalizedMessage();
            msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Dados não foram Salvos com sucesso!", null);
            FacesContext.getCurrentInstance().addMessage("add", msg);
		}
	}
	
	public void atualizarInfo() throws Exception {

        try {
            infDAO.updateInfo(selectedInfo);
            msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Dados atualizados com sucesso!", null);
            FacesContext.getCurrentInstance().addMessage("add", msg);
        } catch (Exception e) {
            e.printStackTrace();
            e.getMessage();
            e.getLocalizedMessage();
            msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Dados não foram atualizados com sucesso!", null);
            FacesContext.getCurrentInstance().addMessage("add", msg);
        }

    }
	
	public void deleteInfo() throws Exception {

        try {
            infDAO.delInfo(selectedInfo);
            msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Dados deletados com sucesso!", null);
            FacesContext.getCurrentInstance().addMessage("add", msg);
        } catch (Exception e) {
            e.printStackTrace();
            e.getMessage();
            e.getLocalizedMessage();
            msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Dados não foram deletados com sucesso!", null);
            FacesContext.getCurrentInstance().addMessage("add", msg);
        }

    }
	/**
	 * Metodo para acionar o componente p:autoComplete para que o usuario
	 * possa vincular o valor a informação
	 * @param Name
	 * @return listInfoForAutoComplete
	 */
	public List<Info> complete(String Name) {
	       if (InfoForAutocomplete == null) {
	    	   InfoForAutocomplete = infDAO.getListarTodos();
	       }
	       for (Info pe : InfoForAutocomplete) {
	            
	           if (pe.getDescricao().contains(Name.toUpperCase())) {
	               pe.setId(pe.getId() == 0 ? null : pe.getId());
	               listInfoForAutoComplete.add(pe);
	           }

	       }
	       return listInfoForAutoComplete;
	   }

	public Long getInfoID() {
		return infoID;
	}

	public void setInfoID(Long infoID) {
		this.infoID = infoID;
	}

	public Info getSelectedInfo() {
		return selectedInfo;
	}

	public void setSelectedInfo(Info selectedInfo) {
		this.selectedInfo = selectedInfo;
	}

	public InfoDAO getInfDAO() {
		return infDAO;
	}

	public void setInfDAO(InfoDAO infDAO) {
		this.infDAO = infDAO;
	}

	public FacesMessage getMsg() {
		return msg;
	}

	public void setMsg(FacesMessage msg) {
		this.msg = msg;
	}

	public List<Info> getInfoForAutocomplete() {
		return InfoForAutocomplete;
	}

	public void setInfoForAutocomplete(List<Info> infoForAutocomplete) {
		InfoForAutocomplete = infoForAutocomplete;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<Info> getListInfoForAutoComplete() {
		return listInfoForAutoComplete;
	}

	public void setListInfoForAutoComplete(List<Info> listInfoForAutoComplete) {
		this.listInfoForAutoComplete = listInfoForAutoComplete;
	}
	
	

}
