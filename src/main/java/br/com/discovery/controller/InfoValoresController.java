package br.com.discovery.controller;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.component.chart.renderer.LineRenderer;
import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.DateAxis;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.LineChartSeries;






import br.com.discovery.dao.InfoValoresDAO;
import br.com.discovery.model.InfoValores;

@ManagedBean
@ViewScoped
public class InfoValoresController implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6369856809961983742L;
	private Long idValores;
	private InfoValores selectedValores;
	private InfoValoresDAO valDAO;
	public FacesMessage msg;
	private List<InfoValores> listOfValores;
	private LineChartModel model;
	private LineChartModel linearModel;
	private Date menorData;
	
	public InfoValoresController(){
		selectedValores = new InfoValores();
		valDAO = new InfoValoresDAO();
		listOfValores = new ArrayList<InfoValores>();
		System.out.println("Incializando Objetos de InfoValores...");
	}
	 @PostConstruct
	    public void init() {
	        createLineModels();
	    }
	
	/**
	 * Metodo para listar todos os valores cadastrados no
	 * banco de dados;
	 * Automaticametne já é dado a lista os valores já que no
	 * construtor é setado uma lista de objetos de InfoValores
	 * vazia.
	 * @return listaofValores
	 */
	public List<InfoValores> getListaDeValores(){
		if(listOfValores == null || listOfValores.isEmpty()){
			listOfValores = valDAO.getListarTodos();
		}
		return listOfValores;
	}
	/**
	 * Metodo para limpar o formulario e o usuário possa inserir um novo dado
	 */
	public void cleanNewInfoValores(){
		selectedValores = null;
		FacesContext.getCurrentInstance().addMessage("add", new FacesMessage(FacesMessage.SEVERITY_INFO, "Pode adicionar uma nova Informação!", null));
		selectedValores = new InfoValores();
	}
	/**
	 * Metodo para salvar o objeto
	 */
public void salvarValor(){
		
		try{
			valDAO.addInfo(selectedValores);
			msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Dados salvos com sucesso!", null);
            FacesContext.getCurrentInstance().addMessage("add", msg);
            //Metodo abaixo é realizado a limpeza do formulario quando salva.
            cleanNewInfoValores();
		}catch(Exception e ){
			e.printStackTrace();
			e.getMessage();
            e.getLocalizedMessage();
            msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Dados não foram Salvos com sucesso!", null);
            FacesContext.getCurrentInstance().addMessage("add", msg);
		}
	}

public void createLineModels(){
	linearModel = initLineModel();
    linearModel.setTitle("Variação do Dólar");
    linearModel.setLegendPosition("e");
    Axis yAxis = linearModel.getAxis(AxisType.Y);
	Axis xAxis = linearModel.getAxis(AxisType.X);
	
}

public LineChartModel  initLineModel(){
	
	linearModel = new LineChartModel();
	String dataAtual;
	LineChartSeries series1 = new LineChartSeries();
	List<InfoValores> dados = getListaDeValores();
	series1.setLabel("Dólar");
	for (int x=0;x < dados.size();x++ ){
		if (menorData == null){
			menorData = dados.get(x).getDatacadastro();
		}
		//Para assegurar que a menor data será atribuido a váriavel
		if (menorData.after(dados.get(x).getDatacadastro())){
			menorData = dados.get(x).getDatacadastro();
		}
		dataAtual = new SimpleDateFormat("yyyy-mm-dd").format(dados.get(x).getDatacadastro());
		series1.set(dados.get(x).getDatacadastro().toString(),dados.get(x).getValorinfo());
		
	}
	
	linearModel.addSeries(series1);
	linearModel.setZoom(true);
	linearModel.getAxis(AxisType.Y).setLabel("Dólar");
	/** Adicionando o formato de informações do tipo data ao eixo X 
	 * da Classe Axis.
	 */
	DateAxis axis = new DateAxis("Datas");
	System.out.println("Menor Data: "+menorData);
	axis.setMin(menorData.toString());
	/**
	 * O set Tick FOrmat é o formato que vai aparece no gráfico, neste caso é o brasilseiro
	 * e pode ser encontrado em http://www.jqplot.com/docs/files/plugins/jqplot-dateAxisRenderer-js.html
	 */
	axis.setTickFormat("%#d %#m %Y");
	linearModel.getAxes().put(AxisType.X,axis);
	
	
	
	return linearModel;
	
}
	public Long getIdValores() {
		return idValores;
	}

	public void setIdValores(Long idValores) {
		this.idValores = idValores;
	}

	public InfoValores getSelectedValores() {
		return selectedValores;
	}

	public void setSelectedValores(InfoValores selectedValores) {
		this.selectedValores = selectedValores;
	}

	public InfoValoresDAO getValDAO() {
		return valDAO;
	}

	public void setValDAO(InfoValoresDAO valDAO) {
		this.valDAO = valDAO;
	}

	public FacesMessage getMsg() {
		return msg;
	}

	public void setMsg(FacesMessage msg) {
		this.msg = msg;
	}

	public List<InfoValores> getListOfValores() {
		return listOfValores;
	}

	public void setListOfValores(List<InfoValores> listOfValores) {
		this.listOfValores = listOfValores;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public LineChartModel getModel() {
		return model;
	}

	public void setModel(LineChartModel model) {
		this.model = model;
	}

	public LineChartModel getLinearModel() {
		return linearModel;
	}

	public void setLinearModel(LineChartModel linearModel) {
		this.linearModel = linearModel;
	}
	public Date getMenorData() {
		return menorData;
	}
	public void setMenorData(Date menorData) {
		this.menorData = menorData;
	}
	
	
	

}
