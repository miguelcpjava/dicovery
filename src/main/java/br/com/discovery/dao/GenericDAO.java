package br.com.discovery.dao;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

/**
 * 
 * @author Miguel Lima
 * @since 10/06/2015
 * @description Classe generica responsavel pela as transações
 * Crud.
 */
public class GenericDAO {

	protected Session session;
	
	/**
	 * Meotodo para capturar a conexão, ou abrir.
	 * @return Session
	 */
	public Session getSession() {
	       return HibernateUtil.getSessionFactory().openSession();
	    }
	/**
	 * Metodo para adicionar um objeto no banco de dados
	 * @param pojo
	 */
	protected void savingPojo(Serializable pojo) {
	       Session sessao = getSession();
	       try{
	       sessao.getTransaction().begin();
	       sessao.save(pojo);
	       sessao.getTransaction().commit();
	       }finally{
	       sessao.close();
	       }
	   }
	/**
	 * Metodo para atualizar um objeto no banco de dados
	 * @param pojo
	 */
	   protected void updatePojo(Serializable pojo) {
	       Session sessao = getSession();
	       try{
	       sessao.beginTransaction();
	       sessao.update(pojo);
	       sessao.flush();
	       sessao.getTransaction().commit();
	       }catch(Exception e){
	    	   e.printStackTrace();
	       }finally{
	       sessao.close();
	       }
	   }
	   /**
	    * Metodo de Busca de forma generica de acordo com a classe.
	    * @param Searchclass
	    * @param key
	    * @return
	    */
	   protected <T extends Serializable> T gettingPojo(Class<T> Searchclass, Serializable key) {
	       Session sessao = getSession();
	       Serializable ReturnToObject = null;
	       try{
	       sessao.beginTransaction();
	       ReturnToObject = (Serializable) sessao.get(Searchclass, key);
	       }catch(Exception e){
	    	   e.printStackTrace();
	       }finally{
	       //sessao.beginTransaction().commit();
	       sessao.close();
	       }
	       return (T) ReturnToObject;
	   }
	   /**
		 * Metodo para remoção um objeto no banco de dados
		 * @param pojo
		 */
	   protected void removePojo(Serializable pojotoRemove) {
	       Session sessao = getSession();
	       try{
	           sessao.getTransaction().begin();
	           sessao.delete(pojotoRemove);
	           sessao.getTransaction().commit();
	       }catch(Exception e){
	           e.printStackTrace();
	       }finally{
	           sessao.close();
	       }
	   }
	   protected <T extends Serializable> List<T> getCleanListOfObjects(Class<T> classToCast, String query) {
	       Session sessao = getSession();
	       List returnObject = null;
	       try{
	       sessao.getTransaction().begin();
	       Query qr = sessao.createQuery(query);

	        returnObject = qr.list();

	       sessao.getTransaction().commit();
	       }catch(Exception e){
	    	   e.printStackTrace();
	       }finally{
	       sessao.close();
	       }
	       return returnObject;
	   }
}
