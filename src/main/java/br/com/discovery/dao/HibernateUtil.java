package br.com.discovery.dao;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * Hibernate Utility class with a convenient method to get Session Factory
 * object.
 * @author Miguel Lima
 *
 */
public class HibernateUtil {
private static final SessionFactory sessionFactory;
    
    static {
        try {
            // Create the SessionFactory from standard (hibernate.cfg.xml) 
            // config file.
            sessionFactory =  new Configuration()
                    .configure("hibernate.cfg.xml")
                    
                    .buildSessionFactory();
           // Configuration conf = new Configuration(); 
            //conf.configure("hibernate.cfg.xml"); 
            //ServiceRegistry serviceRegistry = new ServiceRegistryBuilder().applySettings(conf.getProperties()).build();
            //sessionFactory = conf.buildSessionFactory(serviceRegistry);


        } catch (Throwable ex) {
            // Log the exception. 
            System.err.println("Initial SessionFactory creation failed." + ex);
            System.out.println(ex.getCause());
            System.out.println(ex.getMessage());
            throw new ExceptionInInitializerError(ex);
            
        }
    }
    
    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

}
