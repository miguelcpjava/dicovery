package br.com.discovery.dao;

import java.io.Serializable;


import java.util.List;

import br.com.discovery.model.InfoValores;

public class InfoValoresDAO extends GenericDAO implements Serializable{

	/**
	 * Classe de Acesso ao dado do obejeto da classe InfoValores
	 */
	private static final long serialVersionUID = 1958093199510254320L;
	
	public String addInfo(InfoValores info){
		savingPojo(info);
		return String.valueOf(info.getId());
	}
	
	public void updateInfo(InfoValores info){
		updatePojo(info);
	}
	public void delInfo(InfoValores info){
		removePojo(info);
	}
	public InfoValores getInfoValoresByID(Long idInfo){
		InfoValores info = (InfoValores) gettingPojo(InfoValores.class, idInfo);
		return info;
	}
	public List<InfoValores> getListarTodos(){
		return getCleanListOfObjects(InfoValores.class, "from InfoValores valores");
	}

}
