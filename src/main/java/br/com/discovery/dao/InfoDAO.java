package br.com.discovery.dao;

import java.io.Serializable;
import java.util.List;

import br.com.discovery.model.Info;

public class InfoDAO extends GenericDAO implements Serializable {

	/**
	 * Classe de acesso ao dado do objeto da classe info
	 */
	private static final long serialVersionUID = 7169882603409702967L;

	public InfoDAO() {
		
	}
	
	public String addInfo(Info info){
		savingPojo(info);
		return String.valueOf(info.getId());
	}
	
	public void updateInfo(Info info){
		updatePojo(info);
	}
	public void delInfo(Info info){
		removePojo(info);
	}
	public Info getInfoByID(Long idInfo){
		Info info = (Info) gettingPojo(Info.class, idInfo);
		return info;
	}
	public List<Info> getListarTodos(){
		return getCleanListOfObjects(Info.class, "from Info info");
	}
	

}
