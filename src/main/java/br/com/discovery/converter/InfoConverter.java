package br.com.discovery.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

import br.com.discovery.dao.InfoDAO;
import br.com.discovery.model.Info;

@FacesConverter(value="infoConverter", forClass=Info.class)
public class InfoConverter implements Converter{

	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String arg2) {
		if (arg2 !=null && arg2.trim().length() > 0){
		Long codigo = Long.valueOf(arg2);
			try{
				InfoDAO infDAO = new InfoDAO();
				return infDAO.getInfoByID(codigo);
			}catch(Exception e){
				throw new ConverterException("Não foi possível encontrar devido a "+ arg2+ ", "+e.getMessage());
			}
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {
		if( arg2 == null){
			return "";
		}
		//Carrega o ID do Objeto
		Info info = (Info) arg2;
		return String.valueOf(info.getId());
	}

}
